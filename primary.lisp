;; These are theoretically comments.
;; This is written in vim btw.

(+ 1 2)

(defparameter *nodes*
  '((living-room (you are in the living-room. A wizzard is snoring on the couch.))
   (garden (you are in a beautiful garden. There is a well in front of you))
   (attic (you are in the attic. There is a giant welding torch in the corner.))))

(defparameter *edges*
  '((living-room (garden west door)
		 (attic upstairs ladder))
    (garden (living-room east door))
    (attic (living-room downstairs ladder))))

(defun describe-path (edge)
  `(there is a ,(caddr edge) going ,(cadr edge) from here.))
(defun describe-paths (location edges)
  (apply #'append (mapcar #'describe-path (cdr (assoc location edges)))))

(defun describe-location (location nodes)
  (cadr (assoc location nodes)))

(describe-location 'living-room *nodes*)
(describe-path '(garden west door))


